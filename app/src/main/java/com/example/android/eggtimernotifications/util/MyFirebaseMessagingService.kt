package com.example.android.eggtimernotifications.util

import android.app.NotificationManager
import android.util.Log
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService: FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d("Elvis", "From: ${remoteMessage?.from}")

        remoteMessage.data.let {
            Log.d("Elvis", "Message data payload: " + remoteMessage.data)
        }


        // Se o aplicativo estiver em primeiro plano e queremos mostrar a notificação
        // além de receber os dados, precisamos chamar o sendNotification.
        remoteMessage.notification?.let {
            Log.d("Elvis", "Message Notification Body: ${it.body}")
            val notificationManager = ContextCompat.getSystemService(
                applicationContext,
                NotificationManager::class.java
            ) as NotificationManager
            notificationManager.sendNotification(it.body!!, applicationContext)
        }

    }

    override fun onNewToken(token: String) {
        Log.d("elvis", "Refreshed token: $token")
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String) {
        //TODO Enviar token para o servidor.
    }
}